<?php

namespace Kanzler\ImShop;

use \Bitrix\Main\UserTable;
use \Bitrix\Main\GroupTable;
use \Kanzler\Mindbox\AvailableBalanceRequest\CommandForImShop;
use Kanzler\Helper\Util;

class UserAccountInfo
{

    private $data;
    private $error;

    public function __construct()
    {
        try {
            $this->data = json_decode(file_get_contents("php://input"), true);
        } catch (\Exception $exception) {
            $this->error = $exception->getMessage();
        }
    }

    public function getResult(): array
    {

        try {
            $userInfo = $this->getUserInfo(
                $this->data['userIdentifier']
            );
            return $userInfo;

        } catch (\Exception $exception) {
            \Kanzler\Logger\ImShopLogger::getInstance()->saveLogError(
                $this->data,
                $exception->getMessage(),
            );
            return [
                'error' => $exception->getMessage()
            ];
        }
    }

    private static function getUserGroups($userId)
    {
        $groupNames = [];
        $userGroupsIds = UserTable::getUserGroupIds($userId);
        $groupsList = GroupTable::getList([
            'select' => ['ID', 'NAME'],
            'filter' => ['ID' => $userGroupsIds],
        ]);
        while ($group = $groupsList->fetch()) {
            $groupNames[$group['ID']] = $group['NAME'];
        }
        return $groupNames;
    }

    private function getUserInfo($number)
    {
        $result = [];

        $userData = UserTable::getList(array(
            'select' => array('ID', 'PERSONAL_PHONE', 'NAME', 'LAST_NAME', 'EMAIL', 'PERSONAL_GENDER'),
            'filter' => array('=PERSONAL_PHONE' => $number),
        ))->fetch();

        //perhaps site database has a different phone format
        if(!$userData)
            $number = (new \Kanzler\Helper\Util())->phoneFormatToWithSimbols($number);

        $userData = UserTable::getList(array(
            'select' => array('ID', 'PERSONAL_PHONE', 'NAME', 'LAST_NAME', 'EMAIL', 'PERSONAL_GENDER'),
            'filter' => array('=PERSONAL_PHONE' => $number),
        ))->fetch();

        if ($userData) {
                //getting data from MindBox
                $arMindBoxData = (new CommandForImShop())->execute($userData['ID']);

                $arUser = [
                    'id'     => $number,
                    'name'   => $userData['NAME'] . ' ' . $userData['LAST_NAME'],
                    'phone'  => $userData['PERSONAL_PHONE'],
                    'email'  => $userData['EMAIL'],
                    'gender' => $userData['PERSONAL_GENDER'],
                ];

                $result['user'] = array_merge($arUser, $arMindBoxData);

                if ($userGroups = self::getUserGroups($userData['ID'])) {
                    $result['user']['segments'] = $userGroups;
                }
        } else {
            $result['error'] = [
                'message' => 'Пользователя с таким идентификатором не существует'
            ];
        }
        return $result;
    }

}
