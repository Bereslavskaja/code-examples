<?php
declare(strict_types = 1);

namespace Kanzler\Mindbox\AvailableBalanceRequest;

use Kanzler\Helper\Util;
use Mindbox\DTO\V3\Responses\BalanceResponseDTO;

class CommandForImShop
{
    protected $sUnits;

    public function execute (int $nUserId): array
    {
        $objMindBox = (new \Kanzler\Mindbox)->getCustomerBalance($nUserId);
        $nBonuses = (int)($objMindBox['balances'][0]['available'] ?: 0);
        $sUnits = (new \Kanzler\Helper\Util())->getDeclensionOfWords(
            $nBonuses, ['бонус', 'бонуса', 'бонусов']
        );

        return $objMindBox;

        return [
            'bonuses' => $nBonuses,
            'units' => $sUnits,
            'cardNumber' => (string)(
                $objMindBox['discountCards'][0]['ids']['number'] ?: ''
            ),
            'status' =>
                (string)(
                    $objMindBox['customerSegmentations'][0]['segmentation']['name'] ?: ''
                ),
            'statusAll' => $objMindBox['customerSegmentations'],
            'cardColor' => '#402e2c',
        ];
    }
}
