<?php
namespace Kanzler\Pictures;

class ConvertToWebp {

    private static $isPng = true;

    private static function checkFormat($str)
    {
        if ($str === 'image/png')
        {
            self::$isPng = true;

            return true;
        }
        elseif ($str === 'image/jpeg')
        {
            self::$isPng = false;

            return true;
        }
        else return false;
    }

    private static function implodeSrc($arr)
    {
        $arr[count($arr) - 1] = '';
        return implode('/', $arr);
    }

    private static function generateSrc($str)
    {
        $arPath = explode('/', $str);

        if ($arPath[2] === 'resize_cache')
        {
            $arPath = self::implodeSrc($arPath);
            return str_replace('resize_cache/iblock', 'webp/resize_cache', $arPath);
        }
        else
        {
            $arPath = self::implodeSrc($arPath);

            return str_replace('upload/iblock', 'upload/webp/iblock', $arPath);
        }
    }

    public static function getWebp($array, $intQuality = 70)
    {
        $array['NEW_FILE'] = 0;
        if (self::checkFormat($array['CONTENT_TYPE']))
        {
            $array['WEBP_PATH'] = self::generateSrc($array['SRC']);

            if (self::$isPng)
            {
                $array['WEBP_FILE_NAME'] = str_replace('.png', '.webp', strtolower($array['FILE_NAME']));
            }
            else
            {
                $array['WEBP_FILE_NAME'] = str_replace('.jpg', '.webp', strtolower($array['FILE_NAME']));
                $array['WEBP_FILE_NAME'] = str_replace('.jpeg', '.webp', strtolower($array['WEBP_FILE_NAME']));
            }

            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_PATH']))
            {
                mkdir($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_PATH'], 0777, true);
            }

            $array['WEBP_SRC'] = $array['WEBP_PATH'] . $array['WEBP_FILE_NAME'];

            if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC']))
            {
                if (self::$isPng)
                {
                    $im = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'] . $array['SRC']);
                }
                else
                {
                    $im = imagecreatefromjpeg($_SERVER['DOCUMENT_ROOT'] . $array['SRC']);
                }

                imagewebp($im, $_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC'], $intQuality);

                imagedestroy($im);

                if (filesize($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC']) % 2 == 1)
                {
                    file_put_contents($_SERVER['DOCUMENT_ROOT'] . $array['WEBP_SRC'], "\0", FILE_APPEND);
                }
                $array['NEW_FILE'] = 1;
            }
        }
        return $array;
    }

    public static function getWebpById($nIDPictures, $intQuality = 70){
        $arFile =  \CFile::GetFileArray($nIDPictures);
        $arResult = self::getWebp($arFile, $intQuality);

        return $arResult;
    }

    public static function resizePict($file, $width, $height, $isProportional = true, $intQuality = 70)
    {
        $file = \CFile::ResizeImageGet($file, array('width'=>$width, 'height'=>$height),
            ($isProportional ? BX_RESIZE_IMAGE_PROPORTIONAL : BX_RESIZE_IMAGE_EXACT), false, false, false, $intQuality);

        return $file['src'];
    }

    public static function getResizeWebp($file, $width, $height, $isProportional = true, $intQuality = 70)
    {
        $file['SRC'] = self::resizePict($file, $width, $height, $isProportional, $intQuality);
        $file = self::getWebp($file, $intQuality);

        return $file;
    }

    public static function getResizeWebpSrc($file, $width, $height, $isProportional = true, $intQuality = 70)
    {
        $file['SRC'] = self::resizePict($file, $width, $height, $isProportional, $intQuality);
        $file = self::getWebp($file, $intQuality);

        return $file['WEBP_SRC'];
    }

    public static function getWebpForFiles($nQuality=70){
        ini_set('max_execution_time', 0);
        $ptime = getmicrotime();

        $obRes = \Bitrix\Main\FileTable::GetList(array(
            'select'  => ['*'],
            //'select'  => array('ID', 'HEIGHT', 'WIDTH', 'FILE_SIZE','SUBDIR','FILE_NAME', 'CONTENT_TYPE'),
            'filter'  => array('>FILE_SIZE'=>0, 'MODULE_ID' => "iblock",
                array(
                    "LOGIC" => "OR",
                    array('CONTENT_TYPE'=>'image/jpeg'),
                    array('CONTENT_TYPE'=>'image/png'),
            )),
            'order'   => array('ID'=>'asc'),
            //'limit'   => 8// файлов за шаг
        ));

        $nCountNewIblockPictures = 0;
        $nCountNewResizePictures = 0;
        $nCountResizeAll = 0;
        while ($arRes = $obRes->fetch())
        {
            $arRes['SRC'] = '/upload/'.$arRes['SUBDIR'].'/'.$arRes['FILE_NAME'];

            if(!file_exists($_SERVER['DOCUMENT_ROOT'].$arRes['SRC']))
                continue;

            $arPicts[] = $arRes;

            $arWebpRes = self::getWebp($arRes, $nQuality);
            $arResult['COUNT_IBLOCK_ALL'] = count($arPicts);
            if ($arWebpRes['NEW_FILE'] == 1) {

                fwrite(STDOUT,
                    print_r('created: '.$arWebpRes['WEBP_SRC'].PHP_EOL, true));

                    $arResult['PICTURES']['NEW_IBLOCK_WEBP'][] = $arWebpRes;
                    $nCountNewPictures++;
                    $arResult['COUNT_IBLOCK_NEW'] = $nCountNewIblockPictures;
            }else{
                //image has already been converted to webp format
                $arResult['PICTURES']['OLD_IBLOCK_WEBP'][] = $arWebpRes;
            }

            //getting webp for folder /upload/resize_cache/iblock/
            $sResizeCacheDirName = $_SERVER['DOCUMENT_ROOT'].'/upload/resize_cache/'.$arRes['SUBDIR'];
            if(file_exists($sResizeCacheDirName)){

                $arDirs = array_slice(scandir($sResizeCacheDirName), 2);

                foreach ($arDirs as $nDir){

                    /*there is no resize for the file in /upload/resize_cache/iblock/
                        it will be created in the next step "getting webp for folder /upload/iblock/"
                    */
                    if(!file_exists($sResizeCacheDirName.'/'.$nDir.'/'.$arRes['FILE_NAME']))
                        continue;

                    $arRes['SRC'] = '/upload/resize_cache/'.
                            $arRes['SUBDIR'].'/'.$nDir.'/'.$arRes['FILE_NAME'];

                    $arWebpResizeRes = self::getWebp($arRes, 100);

                    $nCountResizeAll++;
                    $arResult['COUNT_RESIZE_ALL'] = $nCountResizeAll;

                    if ($arWebpResizeRes['NEW_FILE'] == 1) {

                        fwrite(STDOUT,
                            print_r('created: '.$arWebpResizeRes['WEBP_SRC'].PHP_EOL, true));

                        $arResult['PICTURES']['NEW_IBLOCK_WEBP'][] = $arWebpResizeRes;
                        $nCountNewResizePictures++;
                        $arResult['COUNT_RESIZE_NEW'] = $nCountNewResizePictures;
                    }else{
                        //image has already been converted to webp format
                        $arResult['PICTURES']['OLD_RESIZE_WEBP'][] = $arWebpResizeRes;
                    }
                }
            }
        }
        $arResult['COUNT_ALL_CONVERT'] = 'Всего сконвертировано в webp: '.
            ($arResult['COUNT_IBLOCK_NEW'] + $arResult['COUNT_RESIZE_NEW']);
        $arResult['TIME'] = "За время: ".round(getmicrotime()-$ptime, 3)." секунд";

        return $arResult['COUNT_ALL_CONVERT'].PHP_EOL.$arResult['TIME'].PHP_EOL;
    }

    /**
     * @param $nWidth
     * @param $nHeight
     * @param bool $isProportional
     * @param int $nQuality
     *
     * zagotovka. for selective resize.  Not tested
     */
    public static function reGetWebpForFiles($nWidth, $nHeight, $isProportional = true, $nQuality = 100)
    {
        $sPathForWebp = $_SERVER['DOCUMENT_ROOT'].'/upload/webp/resize_cache/';
        $arPathFoWebp = scandir($sPathForWebp);
        foreach($arPathFoWebp as $sDir){

            $arRazrDir = scandir($sPathForWebp.$sDir);
            foreach($arRazrDir as $sRazrDir){

                if(strpos($sDir, $nWidth.'_'.$nHeight)){
                    $sDirPathOrigin =
                        $_SERVER['DOCUMENT_ROOT'].'/upload/resize_cache/iblock/'.$sDir.'/'.$sRazrDir;
                    $arOriginFilesNames = scandir($sDirPathOrigin);

                    //vse originaly izobrazhenij a bitrix, a v papke webp udaljaem
                    foreach($arOriginFilesNames as $sOriginFileName){

                        //file nesushestvuet v papke bitrix
                        if(!file_exists($sDirPathOrigin.'/'.$sOriginFileName))
                            continue;

                        //udalenie pregnego webp
                        unlink($sPathForWebp.'/'.$sDir.'/'.$sRazrDir.'/'.$sOriginFileName);

                        $obRes = \Bitrix\Main\FileTable::GetList(array(
                            'select'  => array('ID', 'HEIGHT', 'WIDTH', 'FILE_SIZE','SUBDIR','FILE_NAME', 'CONTENT_TYPE'),
                            'filter'  => array('>FILE_SIZE'=>0, 'MODULE_ID' => "iblock", 'FILE_NAME'=>$sOriginFileName,
                                array(
                                    "LOGIC" => "OR",
                                    array('CONTENT_TYPE'=>'image/jpeg'),
                                    array('CONTENT_TYPE'=>'image/png'),
                                )),
                            'order'   => array('ID'=>'asc'),
                            //'limit'   =>  файлов за шаг
                        ));
                        $nCountNewWebP = 0;
                        while ($arRes = $obRes->fetch()) {
                            $arImgFile = $arRes;
                            $arImgFile['SRC'] = $sDirPathOrigin.'/'.$sOriginFileName;

                            self::getWebp($arImgFile, $nQuality);
                            $nCountNewWebP++;
                        }

                    }
                }
            }

        }
    }
}
