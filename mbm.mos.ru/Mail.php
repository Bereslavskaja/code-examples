<?php
namespace ApiMbm\App\Classes;

class Mail
{
    private $from;
    private $from_name = '';
    private $type = 'text/html';
    private $encoding = 'utf-8';
    private $notify = false;
    private $attachments = [];

    /**
     * Конструктор принимающий обратный e-mail адрес
     *
     * @param string $from
     */
    public function __construct(string $from = 'no-reply@mbm.ru')
    {
        $this->from = $from;
    }

    /**
     * Метод изменения обратного e-mail адреса
     *
     * @param string $from
     */
    public function setFrom(string $from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Метод изменения имени в обратном адресе
     *
     * @param string $from_name
     */
    public function setFromName(string $from_name)
    {
        $this->from_name = $from_name;

        return $this;
    }

    /**
     * Метод изменения типа содержимого письма
     *
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Метод устанавливающий параметр нужно ли запрашивать подтверждение письма
     *
     * @param bool $notify
     */
    public function setNotify(bool $notify)
    {
        $this->notify = $notify;

        return $this;
    }

    /**
     * Метод изменения кодировки письма
     *
     * @param string $encoding
     */
    public function setEncoding(string $encoding)
    {
        $this->encoding = $encoding;

        return $this;
    }

    public function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }
    /**
     * Метод отправки письма
     *
     * @param string $to
     * @param string $subject
     * @param string $message
     * @return bool
     */
    public function send(string $to, string $subject, string $message)
    {
        $from = $this->from_name.' <' . $this->from . '>';
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From:$from\r\n";
        $headers .= "Reply-To:$from\r\n";
        if ($this->notify) {
            $headers .= "Disposition-Notification-To:$this->from'\r\n";
        }
        if (is_array($this->attachments) && count($this->attachments) > 0) {
            $boundary = md5(time());
            $headers .= "Content-type: multipart/mixed; boundary=\"$boundary\"\r\n";
            $headers .= "Content-Transfer-Encoding: 7bit\r\n";
            $body = "--$boundary\r\n";
            $body .= "Content-Type: $this->type; charset=\"$this->encoding\"\r\n";
            $body .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $body .= $message."\r\n";

            foreach ($this->attachments as $attachment) {
                $body .= "--$boundary\r\n";
                if (isset($attachment['data_base64']) && $attachment['data_base64'] != '') {
                    $content = $attachment['data_base64'];
                }else {
                    list(, $data) = explode(';', $attachment['data']);
                    list(, $content) = explode(',', $data);
                }
                $body .="Content-Type: application/octet-stream; name=\"{$attachment['name']}\"\r\n";
                $body .="Content-Transfer-Encoding: base64\r\n";
                $body .="Content-Disposition: attachment; filename=\"{$attachment['name']}\"\r\n\r\n";
                $headers .="X-Attachment-Id:".rand(1000, 99999)."\r\n\r\n";
                $body .= chunk_split($content);
            }
            $body .= "--$boundary--";
            return mail($to, $subject, $body, $headers);
        } else {
            $headers .= "Content-type:$this->type; charset=$this->encoding\r\n";
            return mail($to, $subject, $message, $headers);
        }
    }
}