<?php
namespace ApiMbm\App\Controllers;

use ApiMbm\App\Classes\Crm\Event as CrmEvent;
use ApiMbm\App\Classes\Enums\NotificationEnum;
use ApiMbm\App\Models\Notifications\Notification;
use ApiMbm\App\Models\Users\User;
use ApiMbm\App\Models\Base\BaseElementModel;
use ApiMbm\Http\Response\Response;
use ApiMbm\App\Classes\Enums\ErrorEnum;
use ApiMbm\App\Classes\Enums\StatusEnum;
use ApiMbm\App\Classes\Mail;
use Carbon\Carbon;
use CEventLog;
use CFile;

class MailController extends Controller
{
    /**
     * Метод для отправки пиьсма и записи в лог
     *
     * @param array $params
     * @return bool
     */
    public function send(array $params)
    {
        $response = Response::getInstance();

        $sendTo = isset($params['to']) && ($params['to'] != '') ? is_array($params['to']) ? implode(', ', $params['to']) : $params['to'] : null;
        $subject = isset($params['subject']) && ($params['subject'] != '') ? $params['subject'] : '';
        $text = isset($params['text']) && ($params['text'] != '') ? $params['text'] : null;

        if (empty($sendTo)) {
            $response->addError(ErrorEnum::PARAM_ERROR, 'Не задан получатель письма', 'to');
            return $response->abort(StatusEnum::BAD_REQUEST);
        }

        $mail = new Mail();
        $mail->setFromName('MBM.RU');
        $result = $mail->send($sendTo, $subject, $text);

        if (!$result) {
            $logText = "Ошибка | "
                .Carbon::now()->format('d.m.Y H:i:s')." | {$sendTo} | {$subject} | {$text}";
            AddMessage2Log($logText, Mail::class);
        }

        return $result;
    }

    /**
     * Метод для отправки обращения из какой либо формы
     *
     * @param array $params
     * @return bool
     */
    public function sendFeedback(array $params)
    {
        $response = Response::getInstance();

        $to = isset($params['to']) && ($params['to'] != '') ? $params['to'] : null;
        $from = isset($params['from']) && ($params['from'] != '') ? $params['from'] : null;
        $subject = isset($params['subject']) && ($params['subject'] != '') ? $params['subject'] : '';
        $text = isset($params['text']) && ($params['text'] != '') ? $params['text'] : null;
        $name = isset($params['name']) && ($params['name'] != '') ? $params['name'] : null;
        $form = isset($params['form']) && ($params['form'] != '') ? $params['form'] : null;
        $attachments = isset($params['attachments']) && ($params['attachments'] != '') ? $params['attachments'] : null;

        $isBadRequest = false;

        if (!isset($from)) {
            $isBadRequest = true;
            $response->addError(ErrorEnum::PARAM_ERROR, 'Не указан автор письма', 'from');
        } elseif (!filter_var($from, FILTER_VALIDATE_EMAIL)) {
            $isBadRequest = true;
            $response->addError(ErrorEnum::PARAM_ERROR, 'Некорректный email отправителя', 'from');
        }

        if (!isset($to)) {
            $to = 'it@mbm.ru';
        } elseif (!filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $isBadRequest = true;
            $response->addError(ErrorEnum::PARAM_ERROR, 'Некорректный email получателя', 'to');
        }

        if ($isBadRequest) {
            return $response->abort(StatusEnum::BAD_REQUEST);
        }

        $header = "Email для обратной связи: {$from}".PHP_EOL;
        if (isset($name)) {
            $header = $header."Обращение от {$name}".PHP_EOL;
        }
        if (isset($form)) {
            $header = $header."из формы {$form}".PHP_EOL;
        }
        $text = $header.'.'.PHP_EOL.$text;

        //отправка формы на наши адреса
        $mail = new Mail();
        $mail->setFromName('MBM.RU');
        $mail->setType('text/plain');
        if (is_array($attachments)) {
            $mail->setAttachments($attachments);
        }
        $result = $mail->send($to, $subject, $text);

        if ($result) {
            //отправляем пользователю что все ОК
            $mail = new Mail();
            $mail->setFromName('MBM.RU');
            Notification::sendByCodes($userId = User::currentId(), [NotificationEnum::NEW_APPEAL]);
            $sFooter = new BaseElementModel;
            $emailText = 'Ваше обращение на тему «'.$subject.'» принято и находится сейчас в обработке.'.'<br/>'.
                'Обработка, как правило, занимает не более 24 часов в рабочие дни. Пожалуйста, ожидайте.<br/><br/>'.
                'Данное письмо является автоматическим. Отвечать на него не надо.'.'<br/><br/>'.
                $sFooter->getEmailFooter();
            $result = $mail->send($from, 'Ваше обращение принято', $emailText);

            //дублируем отправку также на it@mbm.ru (только контакты)
            if ($form === 'Контакты') {
                $mail->send('it@mbm.ru', $subject, $text);
            }

            //+ пишем это в лог
            CEventLog::Add([
                'SEVERITY' => 'INFO',
                'AUDIT_TYPE_ID' => "Онлайн-форма: {$form}",
                'ITEM_ID' => $userId ?? null,
                'DESCRIPTION' => json_encode(
                    ['name' => $name, 'from' => $from, 'subject' => $subject, 'text' => $text],
                    JSON_UNESCAPED_UNICODE
                )
            ]);
        } else {
            $logText = "Ошибка | "
                .Carbon::now()->format('d.m.Y H:i:s')." | {$to} | {$subject} | {$text}";
            AddMessage2Log($logText, Mail::class);
        }

        return $result;
    }

    /**
     * Метод для отправки заявок акселератора
     *
     * @param array $params
     * @return bool
     */
    public function sendAccelerator(array $params)
    {
        $to = 'laskovetssv@fundsp.ru; bogdanovdd@fundsp.ru; accelerator@mbm.ru';
        $subject = 'Подача заявки на участие в отборе в Акселераторе социальных проектов';

        $user = User::current();
        $attachments = isset($params['attachments']) && ($params['attachments'] != '') ? $params['attachments'] : null;

        $mail = new Mail();
        $mail->setFromName('MBM.RU');
        $mail->setType('text/plain');
        $text = 'Заявка от ' . $user['EMAIL'] . ' (' . $user['LAST_NAME'] . ' ' . $user['NAME'] . ' ' . $user['SECOND_NAME'] . ')';
        if (isset($attachments[0]) && $attachments[0]['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            $mail->setAttachments($attachments);
            $result = $mail->send($to, $subject, $text);
            if (!$result) {
                $logText = "Ошибка | "
                    .Carbon::now()->format('d.m.Y H:i:s')." | {$to} | {$subject} | {$text}";
                AddMessage2Log($logText, Mail::class);
                return false;
            }
            return true;
        }

        return false;
    }

    /**
     * Метод для отправки информации о тренингах
     *
     * @param array $params
     * @return bool
     */
    public function sendTrainingInfo(array $params)
    {
        $arUser = User::current();
        $to      = $arUser['EMAIL'];
        $subject = 'Информация о тренингах';
        $arFile = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"].
            "/upload/static/informaciya_o_treningah_po_programmam_obucheniya.docx");

        $attachments[0] = $arFile;
        $attachments[0]['data_base64'] = base64_encode(file_get_contents($arFile['tmp_name']));

        $sFooter = new BaseElementModel;
        $message    = 'Направляем  информацию о тренингах, проводимых по программам обучения АО «Корпорация «МСП» на территории г. Москвы.'.'<br/>'.
            'Подробную информацию по каждому тренингу Вы можете найти во вложении к этому письму.<br/><br/>'.
            $sFooter->getEmailFooter();

        $mail = new Mail();
        $mail->setFromName('MBM.RU');
        $mail->setAttachments($attachments);

        $result = $mail->send($to, $subject, $message);
        if (!$result) {
            $logText = "Ошибка | "
                .Carbon::now()->format('d.m.Y H:i:s')." | {$to} | {$subject} | {$message}";
            AddMessage2Log($logText, Mail::class);
            return false;
        }

        //оповещение пользователя в колокольчик
        Notification::sendByCodes($userId = User::currentId(), [NotificationEnum::TRAINING_INFO]);

        //добавление в CRM
        (new CrmEvent([]))->add(true);

        //отправка формы на наши адреса
        $to = 'at01nf@yandex.ru; bereslavskajam@mos-team.ru';
        $subject = 'Информация о тренингах корпорации МСП';

        $nINN = 'нет';
        if ($arUser['UF_INN'] !== '')
            $nINN = $arUser['UF_INN'];

        $text = 'Проинформирован новый клиент.'.'<br/><br/>'.
            'Дата и время: '.Carbon::now()->format('d.m.Y H:i').'<br/>'.
            'ФИО: '.$arUser['LAST_NAME'].' '.$arUser['NAME'].' '.$arUser['SECOND_NAME'].'<br/>'.
            'Почта: '.$arUser['EMAIL'].'<br/>'.
            'Телефон: '.$arUser['PERSONAL_PHONE'].'<br/>'.
            'ИНН: '.$nINN.'<br/><br/>'.
        $sFooter->getEmailFooter();

        $mail->setAttachments([]);
        $mail->setType('text/html');

        $mail->send($to, $subject, $text);

        return $result;
    }
}