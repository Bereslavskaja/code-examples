<?php
use Arrilot\BitrixMigrations\BaseMigrations\BitrixMigration;
use Arrilot\BitrixMigrations\Exceptions\MigrationException;

class AddTrainingNotifications20191112164008814689 extends BitrixMigration
{
    public static $ntf = [
        'CODE' => 'TRAINING_INFO',
        'NAME' => 'Информация о тренингах',
        'DETAIL_TEXT' => 'Направляем  <a class="adm-link__arrow" target="_blank" rel="norefferer" href="https://dp.mbm.ru/upload/static/informaciya_o_treningah_po_programmam_obucheniya.docx">информацию о тренингах</a>, проводимых по программам обучения АО «Корпорация «МСП» на территории г.&nbsp;Москвы.',
    ];

    /**
     * Run the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function up()
    {
        $niBlockId = $this->getIblockIdByCode('notifications', 'notifications_mbm');
        $objIblockElement = new CIBlockElement;

        if ($arElement = $objIblockElement->GetList(
            [],
            ['IBLOCK_ID' => $niBlockId, 'CODE' => self::$ntf['CODE']],
            false,
            ['IBLOCK_ID', 'ID', 'CODE'])->Fetch()
        ) {
            $nElementId = $arElement['ID'];
            $objIblockElement->Update($nElementId, self::$ntf);
        } else {
            $arFields = self::$ntf;
            $arFields['IBLOCK_ID'] = $niBlockId;
            $objIblockElement->Add($arFields);
        }
    }

    /**
     * Reverse the migration.
     *
     * @return mixed
     * @throws \Exception
     */
    public function down()
    {
        $niBlockId = $this->getIblockIdByCode('notifications', 'notifications_mbm');

        $idRes = CIBlockElement::GetList([], [
            'IBLOCK_ID' => $niBlockId,
            'CODE' => self::$ntf['CODE'],
        ])->Fetch();
        if (isset($idRes))
            CIBlockElement::Delete($idRes['ID']);
    }
}