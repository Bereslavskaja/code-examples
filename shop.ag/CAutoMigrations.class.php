<?php
/*
Автоматическое исполнение файлов новых/изменённых миграций
(в переменных окружения можно включать или выключать автомиграции)
класс автомиграций:
*/
namespace AutoMigrations;
/**
 * Class CAutoMigrations
 * @package AutoMigrations
 * Класс для автоматического/ручного выполнения миграций БД
 */

class CAutoMigrations {
    /**
     * @param $mode - способ выполнения миграций
     * @return массив с информациией о блокировке системы исполнения миграций,
     * либо массив со списком автоматически исполненных миграций
     */
    function automigrations($mode){

    $sBranch =  getenv(PROJECT_GIT_BRANCH);

    //проверка на блокировку другим процессом запуска автомиграций
    $arLockedBy = (file($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/.lock", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));

    //не заблокировано другим процессом миграций?
    if(file_exists($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/.lock")
      && $mode=='auto_manual'){
        $arResult['errors'] = '<div class="mig-error">Migrations service are temporarily locked by
              another migrations process. Please try again later.</div>';
        return $arResult;
    }

    if (!file_exists($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/.lock")){

      $resDir = opendir($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch");
      $arMigs = array();

      while($filename = readdir($resDir)){
          if($filename =='.' || $filename =='..')continue;
          if(!preg_match("#\.log$#",$filename) && !preg_match("#\.mig$#",$filename))continue;
          if(is_dir($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$filename"))continue;

          $cross_name = substr($filename, 0, -4);

          //достать хэш из логфайла
          if(preg_match("#\.log$#",$filename)){
            $lines = file($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/$filename");
            $hash_log = substr($lines[count($lines)-1], -33);
            //убираю символ переноса строки в конце строки
            $hash_log = substr($hash_log, 0, -1);
            $arMigs[$cross_name]['hash_log'] = $hash_log;
            unset($hash_log);
          }

          //получить хэш миграции
          if(preg_match("#\.mig$#",$filename)){
            $hash_file = md5_file($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/$filename");
            $arMigs[$cross_name]['hash_file'] = $hash_file;
            unset($hash_file);
          }

          $arMigs[$cross_name][] = $filename;
      }
      ksort($arMigs);

      //запуск необходимых миграций с записью в логфайл
      foreach($arMigs as $key=>$arMig){
        if(isset($arMig['hash_log']) && $arMig['hash_log'] == $arMig['hash_file']){
          continue;
        }

        $arRunnedMigs[] = $arMig;
        $arResult = $arRunnedMigs;

        //перед запуском первой миграции и до завершения последней, будет жить файл блокировки .lock
        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/.lock", $mode."\n", LOCK_EX);
        include ($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/$key.mig");

        $log_text = date("d.m.Y H:i:s ").$mode." ".$arMig['hash_file']."\n";
        file_put_contents($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/$key.log", $log_text, FILE_APPEND | LOCK_EX);
      }
        unlink($_SERVER["DOCUMENT_ROOT"]."/local/.migrations/migs/$sBranch/.lock");
        if($mode == 'auto_manual'){
          return $arResult;
        }
    }
  }
}
