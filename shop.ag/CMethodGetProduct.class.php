<?php
    namespace Method;
    require_once("CMethod.class.php");
    require_once(__DIR__."/../../CShop/COffer.class.php");
    require_once(__DIR__."/../../CShop/CProduct.class.php");
    require_once(__DIR__."/../../CShop/CSection.class.php");
    require_once(__DIR__."/../../CShop/CStore.class.php");
    require_once(
        $_SERVER["DOCUMENT_ROOT"]
        ."/local/libs/classes/Utils/Lang/CLang.class.php"
    );
    use App\Utils\Lang as Lang;
    use Method;
    use App\Shop as Shop;



    /**
    Метод получения информации о товаре
    */
    class CMethodGetProduct extends \Method\CMethod{

        function __construct($sToken){
            parent::__construct($sToken);
            \CModule::IncludeModule("iblock");
            \CModule::IncludeModule('highloadblock');
        }

        function go($arParameters = []){
            $objOffer = new \Shop\COffer;
            $objProduct = new \Shop\CProduct;
            $objSection = new \Shop\CSection;
            $objStore = new \Shop\CStore;

            $nCurrentRegionId = $this->getRegionId();

            $nProductId = intval($this->getParam("id"));
            $sProductCode = trim($this->getParam("code"));
            $onlyActiveOffers = trim($this->getParam("onlyActiveOffers"));//EGCHELYABINSK-1143 11/06/19

            if($nProductId)
                $arProduct = $objProduct->getById($nProductId,$nCurrentRegionId);
            elseif($sProductCode)
                $arProduct = $objProduct->getByCode($sProductCode,$nCurrentRegionId);
            else
                return [];

            $nProductId = $arProduct["ID"];
            $arProperties = $objProduct->getProperties($nProductId);

            if($onlyActiveOffers)$onlyActiveOffers = 'Y';       //EGCHELYABINSK-1143 11/06/19
            else $onlyActiveOffers = '';
            
            $resOffers = \CIBlockElement::GetList(["id"=>"desc"],[
                "IBLOCK_ID"=>$this->getIblockId('offers','offers'),
                "PROPERTY_CML2_LINK"=>$nProductId,
                "ACTIVE" => $onlyActiveOffers//EGCHELYABINSK-1143 11/06/19
            ],false,false,["ID"]);
            $arOffers = [];
            while($arOffer = $resOffers->Fetch())
                $arOffers[] = $objOffer->getInfo($arOffer["ID"]);

            //получение лаек
            $arLikes=[];
            $arHLBlock =
            \Bitrix\Highloadblock\HighloadBlockTable::getById(
                $this->getHLIblockId('alt_likes'))->fetch();
            $obEntity =
            \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
            $strEntityDataClass = $obEntity->getDataClass();

            $res = $strEntityDataClass::getList([
                'select' => ['*'],
                'filter' => ['UF_LIKE_ELEMENT_ID' => $nProductId]
            ]);
            while($arLike = $res->Fetch()){
                $arLikes[] = $arLike;
            }


            $arThumb = \CFile::ResizeImageGet($arProduct["PREVIEW_PICTURE"],[
                "width" =>  THUMBNAIL_WIDTH,
                "height"=>  THUMBNAIL_HEIGHT
            ],BX_RESIZE_IMAGE_PROPORTIONAL);
            $sFull=\CFile::GetPath($arProduct["DETAIL_PICTURE"]);

            $arSection = $objSection->GetById($arProduct["IBLOCK_SECTION_ID"]);

            $arProduct["DETAIL_TEXT"] =
               str_replace("&nbsp;"," ",$arProduct["DETAIL_TEXT"]);

            $arResult = [
                "id"=>$arProduct["ID"],
                "name"=>\Lang\CLang::gt($arProduct["NAME"]),
                "code"=>$arProduct["CODE"],
                "section_id"=>$arSection["ID"],
                "section"=>[
                    "id"=>$arSection["ID"],
                    "name"=>$arSection["NAME"],
                    "code"=>$arSection["CODE"],
                ],
                "active"=>$arProduct["ACTIVE"]=="Y"?true:false,
                "min_price"=>$arProperties["MINIMUM_PRICE"]["VALUE"],
                "max_price"=>$arProperties["MAXIMUM_PRICE"]["VALUE"],
                "newproduct"=>$arProperties["NEWPRODUCT"],
                "saleleader"=>$arProperties["SALELEADER"],
                "specialoffer"=>$arProperties["SPECIALOFFER"],
                "expires"=>$arProperties["EXPIRES"]["VALUE"],
                "annuled_date"=>$arProperties["ANNUL_DATE"]["VALUE"],
                "image"=>[
                    "full"=>$sFull,
                    "thumbnail"=>$arThumb["src"]
                ],
                "description"=>$arProduct["DETAIL_TEXT"],
                "preview_description"=>$arProduct["PREVIEW_TEXT"],
                "receive_rule"=>$arProperties["RECEIVE_RULE"]["VALUE"],
                "cancel_rule"=>$arProperties["CANCEL_RULE"]["VALUE"],
                "type"=>$arProperties["TYPE"]["VALUE"],
                "for_free"=>$arProperties["FOR_FREE"],
                "deny_cancel"=>$arProperties["DENY_CANCEL"],
                "offers"=>$arOffers,
                "likes"=>$arLikes
            ];

            $arResult["newproduct"]
                = $arResult["newproduct"]['VALUE_XML_ID']=='Y'?true:false;
            $arResult["saleleader"]
                = $arResult["saleleader"]['VALUE_XML_ID']=='Y'?true:false;
            $arResult["specialoffer"]
                    = $arResult["specialoffer"]['VALUE_XML_ID']=='Y'?true:false;
            $arResult["for_free"]
                    = $arResult["for_free"]['VALUE_XML_ID']=='Y'?true:false;
            $arResult["deny_cancel"]
                    = $arResult["deny_cancel"]['VALUE_XML_ID']=='Y'?true:false;

            return $arResult;
        }
    }