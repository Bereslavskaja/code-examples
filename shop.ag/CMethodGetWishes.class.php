<?
namespace Method;
require_once("CMethod.class.php");

/**
  Метод для работы с лайками товара каталога
*/
class CMethodGetWishes extends \Method\CMethod{

    function __construct(){
        global $USER;
        $this->UserId = $USER->GetID();

        \CModule::IncludeModule("iblock");
        \CModule::IncludeModule('highloadblock');

        $nIblockId = $this->getHLIblockId('alt_likes');

        $this->arHLBlock = $arHLBlock =
        \Bitrix\Highloadblock\HighloadBlockTable::getById($nIblockId)->fetch();

        $this->obEntity = $obEntity =
        \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arHLBlock);
        $this->strEntityDataClass = $strEntityDataClass = $obEntity->getDataClass();
    }

    //получение всех лайков для всех пользователей
    function getAllWishes($productId) {
        $arLikes=[];

        $res = $this->strEntityDataClass::getList([
            'select' => ['*'],
            'filter' => ['UF_LIKE_ELEMENT_ID' => $productId]
        ]);
        while($arLike = $res->Fetch()){
            $arLikes[] = $arLike;
        }
        if (count($arLikes) > 0){
            return count($arLikes);
        }else{
            return '0';
        }
    }

    //получение одного лайка по ID товара и ID пользователя
    function getWishByID($productId){
        $res = $this->strEntityDataClass::getList([
            'select' => ['*'],
            'filter' => [
                            'UF_LIKE_ELEMENT_ID' => $productId,
                            'UF_LIKE_USER_ID'    => $this->UserId
                        ]
        ]);
        if($arLike = $res->Fetch()){
            return $arLike;
        }
    }

    //добавление лайка
    function addWish($productId){

        if($this->getWishByID($productId)){
            return $this->addError(
                \Lang\CLang::gt('Уже добавлено в желания')
            );
        }

        if(!$result = $this->strEntityDataClass::add(
            [
              'UF_LIKE_ELEMENT_ID' => $productId,
              'UF_LIKE_USER_ID'    => $this->UserId
        ])){
            return $this->addError('Ошибка!');
        }
        return $this->getAllWishes($productId);
    }

    //удаление лайка
    function removeWish($productId){
            //получим ID элемента HL блока для удаления
            if(!$ElementId = $this->getWishByID($productId)){
                return $this->addError(
                    \Lang\CLang::gt('Ошибка удаления из желаний')
                );
            }

            $result = $this->strEntityDataClass::delete($ElementId['ID']);

        return $this->getAllWishes($productId);
    }

    //получение лайков для пользователя по  его id
    function getMyWishes(){
        $res = $this->strEntityDataClass::getList([
            'select' => ['*'],
            'filter' => [
                            'UF_LIKE_USER_ID'    => $this->UserId
                        ]
        ]);
        if($arUserLike = $res->Fetch()){
            return $arUserLike;
        }
    }
}
