<?php
/**
Добавление highloadblock "лайки"
*/

use Bitrix\Main\Loader;
Loader::includeModule("highloadblock");
use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

    //создание hl-блока или обновление
    if (!$hlblock = Hl\HighloadBlockTable::getList([
        'filter' => ['=NAME' => 'LIKES']
    ])->fetch()){
            //нет такого hl блока, добавляем
            $result = Hl\HighloadBlockTable::add(array(
                'NAME' => 'LIKES',
                'TABLE_NAME' => 'alt_likes',
            ));

            if (!$result->isSuccess()) {
                $errors = $result->getErrorMessages();
                echo $errors;
            } else {
                $id = $result->getId();
                $new_hl = true;
            }
    }
    else{
            //такой hl блок есть, получим его id
            $id = $hlblock['ID'];
            $new_hl = false;
    }

// Добавление пользовательских свойств
$oUserTypeEntity = new CUserTypeEntity();
$arUserFields    = [
    [
/*
*  Идентификатор сущности, к которой будет привязано свойство.
* Для секция формат следующий - IBLOCK_{IBLOCK_ID}_SECTION
*/
    'ENTITY_ID'         => 'HLBLOCK_'.$id,
/* Код поля. Всегда должно начинаться с UF_ */
    'FIELD_NAME'        => 'UF_LIKE_USER_ID',
    /* Указываем, что тип нового пользовательского свойства строка */
    'USER_TYPE_ID'      => 'integer',
/*
* XML_ID пользовательского свойства.
* Используется при выгрузке в качестве названия поля
*/
'XML_ID'            => 'XML_LIKE_USER_ID',
/* Сортировка */
'SORT'              => 500,
/* Является поле множественным или нет */
'MULTIPLE'          => 'N',
/* Обязательное или нет свойство */
    'MANDATORY'         => 'N',
/*
* Показывать в фильтре списка. Возможные значения:
* не показывать = N, точное совпадение = I,
* поиск по маске = E, поиск по подстроке = S
*/
    'SHOW_FILTER'       => 'N',
/*
* Не показывать в списке. Если передать какое-либо значение,
* то будет считаться, что флаг выставлен.
*/
    'SHOW_IN_LIST'      => '',
/*
* Не разрешать редактирование пользователем.
* Если передать какое-либо значение, то будет считаться,
* что флаг выставлен.
*/
    'EDIT_IN_LIST'      => '',
    /* Значения поля участвуют в поиске */
    'IS_SEARCHABLE'     => 'N',
    /*
* Дополнительные настройки поля (зависят от типа).
* В нашем случае для типа string
*/
    'SETTINGS'          => array(
        /* Значение по умолчанию */
        'DEFAULT_VALUE' => '',
        /* Размер поля ввода для отображения */
        'SIZE'          => '20',
        /*Минимальное значение (0 - не проверять) */
        'MIN_VALUE'    => '0',
        /* Максимальное значение (0 - не проверять) */
        'MAX_VALUE'    => '0',
        //Точность (количество знаков после запятой
        'PRECISION'     => '0'
    ),
    /* Подпись в форме редактирования */
        'EDIT_FORM_LABEL'   => array(
            'ru'    => 'ID пользователя',
            'en'    => 'User ID',
        ),
    /* Заголовок в списке */
        'LIST_COLUMN_LABEL' => array(
            'ru'    => 'ID пользователя',
            'en'    => 'User ID',
        ),
    /* Подпись фильтра в списке */
        'LIST_FILTER_LABEL' => array(
            'ru'    => 'ID пользователя',
    'en'    => 'User ID',
    ),
    /* Сообщение об ошибке (не обязательное) */
        'ERROR_MESSAGE'     => array(
            'ru'    => 'Ошибка при заполнении пользовательского свойства User ID',
            'en'    => 'An error in completing the user field',
    ),
    /* Помощь */
        'HELP_MESSAGE'      => array(
            'ru'    => '',
    'en'    => '',
        ),
    ],
    [
        'ENTITY_ID'         => 'HLBLOCK_'.$id,
    /* Код поля. Всегда должно начинаться с UF_ */
        'FIELD_NAME'        => 'UF_LIKE_ELEMENT_ID',
        /* Указываем, что тип нового пользовательского свойства строка */
        'USER_TYPE_ID'      => 'integer',
    /*
    * XML_ID пользовательского свойства.
    * Используется при выгрузке в качестве названия поля
    */
    'XML_ID'            => 'XML_LIKE_ELEMENT_ID',
    /* Сортировка */
    'SORT'              => 600,
    /* Является поле множественным или нет */
    'MULTIPLE'          => 'N',
    /* Обязательное или нет свойство */
        'MANDATORY'         => 'N',
    /*
    * Показывать в фильтре списка. Возможные значения:
    * не показывать = N, точное совпадение = I,
    * поиск по маске = E, поиск по подстроке = S
    */
        'SHOW_FILTER'       => 'N',
    /*
    * Не показывать в списке. Если передать какое-либо значение,
    * то будет считаться, что флаг выставлен.
    */
        'SHOW_IN_LIST'      => '',
    /*
    * Не разрешать редактирование пользователем.
    * Если передать какое-либо значение, то будет считаться,
    * что флаг выставлен.
    */
        'EDIT_IN_LIST'      => '',
        /* Значения поля участвуют в поиске */
        'IS_SEARCHABLE'     => 'N',
        /*
    * Дополнительные настройки поля (зависят от типа).
    * В нашем случае для типа string
    */
        'SETTINGS'          => array(
            /* Значение по умолчанию */
            'DEFAULT_VALUE' => '',
            /* Размер поля ввода для отображения */
            'SIZE'          => '20',
            /*Минимальное значение (0 - не проверять) */
            'MIN_VALUE'    => '0',
            /* Максимальное значение (0 - не проверять) */
            'MAX_VALUE'    => '0',
            //Точность (количество знаков после запятой
            'PRECISION'     => '0'
        ),
        /* Подпись в форме редактирования */
            'EDIT_FORM_LABEL'   => array(
                'ru'    => 'ID товара',
                'en'    => 'Element ID',
            ),
        /* Заголовок в списке */
            'LIST_COLUMN_LABEL' => array(
                'ru'    => 'ID товара',
                'en'    => 'Element ID',
            ),
        /* Подпись фильтра в списке */
            'LIST_FILTER_LABEL' => array(
                'ru'    => 'ID товара',
        'en'    => 'Element ID',
        ),
        /* Сообщение об ошибке (не обязательное) */
            'ERROR_MESSAGE'     => array(
                'ru'    => 'Ошибка при заполнении пользовательского свойства Element ID',
                'en'    => 'An error in completing the user field',
        ),
        /* Помощь */
            'HELP_MESSAGE'      => array(
                'ru'    => '',
        'en'    => '',
            ),
    ]
];

//новый hl блок, добавляем поля
if ($new_hl == true) {
    foreach ($arUserFields as $arUserField) {
        $iUserFieldId = $oUserTypeEntity->Add($arUserField);
    }
}
//не новый hl блок, обновляем поля
if ($new_hl == false) {
    foreach ($arUserFields as $arUserField) {
        $iUserFieldId = $oUserTypeEntity->Update('HLBLOCK_'.$id, $arUserField);
    }
}