<?
require_once($_SERVER["DOCUMENT_ROOT"]
    ."/local/libs/classes/Backend/CMethod/CMethodGetWishes.class.php"
);

$objWish = new \Method\CMethodGetWishes;

if ($_REQUEST['wish'] == 'on'){$ActionOnItem = 'addWish';}
if ($_REQUEST['wish'] == 'off'){$ActionOnItem = 'removeWish';}

$arAnswer = 0;

global $USER;

if(!$USER->IsAuthorized()){
    $objWish->addError(
        \Lang\CLang::gt('Необходимо авторизоваться!')
    );
}
elseif(intval($_REQUEST['productid']) > 0 && isset($ActionOnItem)){
    $arAnswer = $objWish->$ActionOnItem($_REQUEST['productid']);
}

if(!$objWish->getErrors()){
    $arResult = ["errors"=>[], "result"=>$arAnswer];
}
else{
    $arResult = ["errors"=>$objWish->getErrors()];
}

echo json_encode($arResult);

require(
$_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php"
);
