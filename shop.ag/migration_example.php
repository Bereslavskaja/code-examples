<?php

    /**
     *  Создание инфоблока социальных сетей и его предустановленных данных
     * 
    */
    CModule::IncludeModule("iblock");
    $sIBlockCode = 'soc_services';
    $sIblockType = 'references';
    $arFields = [
        "SITE_ID"=>"s1",
        "IBLOCK_TYPE_ID"=>$sIblockType,
        "CODE"=>$sIBlockCode,
        "NAME"=>"Социальные сервисы",
        "ACTIVE"=>"Y",
        "GROUP_ID"=>[1=>"X",2=>"R",7=>"W"]
    ];    
    
    $objIBlock = new CIblock;

    // Если инфоблок ещё не создан - создаём
    $objIBLock = new CIBlock;
    if(!$arIBlock = CIBlock::GetList([],[
        "TYPE"=>$sIblockType,
        "CODE"=>$sIBlockCode
    ])->Fetch()){
        if(!$nIblockId = $objIBLock->Add($arFields)){
            echo $objIBLock->LAST_ERROR;
            die;
        }
    }
    else{
        $objIBLock->Update($arIBlock["ID"],$arIBlock);
        $nIblockId = $arIBlock["ID"];
    }
    
    //Установка полей инфоблока
    CIBlock::SetFields(
        $nIblockId,
        [
            "PREVIEW_TEXT_TYPE" => [
						"DEFAULT_VALUE" => "html"
						],
			
			"DETAIL_TEXT_TYPE" => [
						"DEFAULT_VALUE" => "html"
						],			
			
			"CODE"=>[
				"IS_REQUIRED" => "Y",
                "DEFAULT_VALUE"=>[
					"UNIQUE"=>"Y",
                    "TRANSLITERATION"=>"Y",
					"TRANS_CASE"=>"L",
                    "TRANS_SPACE"=>"_",
                    "TRANS_OTHER"=>"_",
                    "TRANS_EAT"=>"Y",
                ]
            ],
        ]        
    );
    
    // Если нет свойства "Ссылка на сервис" - создаём, иначе обновляем
    $objIblockProperty = new CIBlockProperty;
    $arFields = [
        "CODE"  =>  "URL",
        "NAME"  =>  "Cсылка на сервис",
        "IBLOCK_ID"=>$nIblockId,
        "PROPERTY_TYPE"=>"S"
    ];
    if(!$arIblockProperty = $objIblockProperty->GetList([],[
        "IBLOCK_ID"=>$arFields["IBLOCK_ID"],
        "CODE"=>$arFields["CODE"],
    ])->Fetch()){
        $objIblockProperty->Add($arFields);
    }
    else{
        $objIblockProperty->Update($arIblockProperty["ID"],$arFields);
    }    
    
    //Добавление демо-данных
    $objIblockElement = new CIBlockElement;
    $arElements = [
            [
                "IBLOCK_ID"=>$nIblockId,
                "IBLOCK_SECTION_ID" => false,          // элемент лежит в корне раздела
                "ACTIVE"            => "Y",
                "SORT"              =>  100,    
                "NAME"              =>  "Facebook",
                "CODE"              =>  "facebook",
                "PREVIEW_PICTURE"   =>  CFile::MakeFileArray(__DIR__."/data/images/icon-facebook.png"),
                "PROPERTY_VALUES"   =>  [
                        "URL"   => "facebook.com",
                    ],  
            ],
            [
                "IBLOCK_ID"=>$nIblockId,
                "IBLOCK_SECTION_ID" => false,        
                "ACTIVE"            => "Y",
                "SORT"              =>  200,
                "NAME"              =>  "Вконтакте",
                "CODE"              =>  "vk", 
                "PROPERTY_VALUES"   =>  [
                        "URL"   => "vk.com",
                    ],
            ],        
            [   
                "IBLOCK_ID"=>$nIblockId,
                "IBLOCK_SECTION_ID" => false,         
                "ACTIVE"            => "Y",
                "SORT"              =>  300,
                "NAME"              =>  "Twitter",
                "CODE"              =>  "twitter", 
                "PROPERTY_VALUES"   =>  [
                        "URL"   => "twitter.com",
                    ],
            ],
            [    
                "IBLOCK_ID"=>$nIblockId,
                "IBLOCK_SECTION_ID" => false,          
                "ACTIVE"            => "Y",
                "SORT"              =>  400,
                "NAME"              =>  "Одноклассники",
                "CODE"              =>  "odnoklassniki", 
                "PROPERTY_VALUES"   =>  [
                        "URL"   => "ok.ru",
                    ],                    
            ],           
        ];
    foreach($arElements as $arElementFields){
        if($arElement = $objIblockElement->GetList(
            [],
            ["IBLOCK_ID"=>$arElementFields["IBLOCK_ID"], "CODE"=>$arElementFields["CODE"]],
            false,
            ["IBLOCK_ID", "ID","CODE"])->Fetch()){
                $nElementId = $arElement["ID"];
                $objIblockElement->Update($nElementId,$arElementFields);
            }
            else{
                if(!$objIblockElement->Add($arElementFields)){
                    echo $objIblockElement->LAST_ERROR;
                    echo "<pre>";
                    print_r($arElementFields);
                    echo "</pre>";
                    die;
                }
            }        
    }        
