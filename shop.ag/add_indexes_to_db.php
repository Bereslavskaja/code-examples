<?php
//Добавление индексов в бд для ускорения работы запросов
global $DB;

if($DB->Connect($DBHost, $DBName, $DBLogin, $DBPassword)){
  foreach ($arData as $arTable) {
    $result = $DB->Query("SHOW TABLES LIKE '".$arTable["table_name"]."';");

    if($row = $result->Fetch()){
      //сбор всех индексов для таблицы и группировка их по key_name
      $result = $DB->Query("SHOW KEYS FROM `".$arTable["table_name"]."`;");
      while($row = $result->Fetch()){
        $arTempSort[$row['Key_name']][] = $row['Column_name'];
      }

      foreach ($arTable['table_fields'] as $arTableFields){
         //проверка существования индексов
         foreach ($arTempSort as $key_name) {
           if(count($key_name) == 1 && in_array($arTableFields["field_name"], $key_name)){
             //такой однопольный индекс уже есть
             $indexIsset = true;
             continue;
           }
         }

         if(!$indexIsset){
            $result = $DB->Query("ALTER TABLE `".$arTable["table_name"]."`
              ADD INDEX `".$arTableFields["field_name"]."` (`".$arTableFields["field_name"]."`);");
         }
         unset($indexIsset);
      }
      unset($arTempSort);
    }
  }
}
else{
    echo 'Нет соединения с базой данных';
}
